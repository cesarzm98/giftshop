﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interaxis.gifttshop.data.Repository
{
    public interface IRepository<T> where T : class
    {
        T Add(T model);
        T Get(Guid id);
        T Update(T model);
        List<T> GetAll();
    }
}
