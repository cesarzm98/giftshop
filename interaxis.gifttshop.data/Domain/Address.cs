﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interaxis.gifttshop.data.Domain
{
    public class Address : ID
    {
        public string Street { get; set; }
        public string Reference { get; set; }
        public string InterNumber { get; set; }
        public string ExternNumber { get; set; }
        public int CP { get; set; }
        public string Location { get; set; }
    }
}
