﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interaxis.gifttshop.data.Domain
{
    public class Sale_Detail : ID
    { 
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public double Total { get; set; }
    }
}
