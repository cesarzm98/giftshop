﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interaxis.gifttshop.data.Domain
{
    public class Sale : ID
    {
        public Address Address { get; set; }
        public string PayType { get; set; }
        public List<Sale_Detail> Sale_Detail { get; set; }
        public DateTime Date { get; set; }
        public double Total { get; set; }
    }
}
