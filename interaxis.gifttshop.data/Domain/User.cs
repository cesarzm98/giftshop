﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interaxis.gifttshop.data.Domain
{
    public class User : ID
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Rol Rol { get; set; }
    }
}
