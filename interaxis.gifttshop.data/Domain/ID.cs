﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interaxis.gifttshop.data.Domain
{
    public class ID
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
    }
}
