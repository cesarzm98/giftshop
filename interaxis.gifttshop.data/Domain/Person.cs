﻿using System;
using System.Collections.Generic;
using System.Text;

namespace interaxis.gifttshop.data.Domain
{
    public class Person : ID
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public User User { get; set; }

    }
}
